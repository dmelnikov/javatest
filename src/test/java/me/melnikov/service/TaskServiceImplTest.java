package me.melnikov.service;

import me.melnikov.model.Car;
import me.melnikov.model.Task;
import me.melnikov.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TaskServiceImplTest {

    @Autowired
    private TaskService taskService;
    @Autowired
    private UserService userService;
    @Autowired
    private CarService carService;

    private Task task;

    @Before
    public void setUp() throws Exception {
        User user = userService.getById(1l);
        Car car = carService.getById(1l);
        task = new Task(user, "repair", car);
        taskService.create(task);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void getAll() {
        List<Task> tasks = taskService.getAll();
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    @Transactional
    @Rollback(true)
    public void getById() {
        Task task = taskService.getById(this.task.getId());
        Assert.assertEquals(this.task.getDescription(), task.getDescription());
        Assert.assertEquals(this.task.getCar().getId(), task.getCar().getId());
        Assert.assertEquals(this.task.getUser().getId(), task.getUser().getId());
    }

    @Test
    @Transactional
    @Rollback(true)
    public void create() {
        User user = userService.getById(1l);
        Car car = carService.getById(1l);
        Task task = new Task(user, "repair", car);
        taskService.create(task);

        List<Task> tasks = taskService.getAll();
        Assert.assertEquals(task.getDescription(), tasks.get(0).getDescription());
    }

    @Test
    @Transactional
    @Rollback(true)
    public void update() {
        User user = userService.getById(2l);
        Car car = carService.getById(2l);
        Task task = taskService.getById(this.task.getId());
        task.setDescription("Test Description");
        task.setUser(user);
        task.setCar(car);

        Task updatedTask = taskService.update(this.task, task);
        assertEquals(updatedTask.getUser().getId(), task.getUser().getId());
        assertEquals(updatedTask.getCar().getId(), task.getCar().getId());
        assertEquals(updatedTask.getDescription(), task.getDescription());
    }

    @Test
    @Transactional
    @Rollback(true)
    public void delete() {
        List<Task> tasks = taskService.getAll();
        Long id = this.task.getId();
        taskService.delete(id);

        Task task = taskService.getById(id);

        assertNull(task);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void itemExist() {
        Boolean bool = taskService.itemExist(this.task.getId());
        assertTrue(bool);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void itemExist1() {
        Boolean bool = taskService.itemExist(this.task);
        assertTrue(bool);
    }
}