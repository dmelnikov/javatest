package me.melnikov.service;

import me.melnikov.model.Car;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CarServiceImplTest {

    @Autowired
    private CarService carService;

    private Car car;

    @Before
    public void setUp() {
        this.car = new Car("Lada", "4x4");
        this.carService.create(car);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void itemExist() {
        Boolean bool = carService.itemExist(this.car.getId());

        assertEquals(true, bool);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void itemExist1() {
        Boolean bool = carService.itemExist(this.car);

        assertEquals(true, bool);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void getAll() {
        List<Car> cars = carService.getAll();
        assertEquals(4, cars.size());
    }

    @Test
    @Transactional
    @Rollback(true)
    public void getById() {
        Car car = carService.getById(this.car.getId());
        assertEquals(this.car.getMark(), car.getMark());
        assertEquals(this.car.getModel(), car.getModel());
    }

    @Test
    @Transactional
    @Rollback(true)
    public void create() {
        Car car = new Car("Opel", "Kadet");
        carService.create(car);

        Car createdCar = carService.getById(car.getId());
        assertEquals(createdCar.getMark(), car.getMark());
        assertEquals(createdCar.getModel(), car.getModel());
    }

    @Test
    @Transactional
    @Rollback(true)
    public void update() {
        Car car = carService.getById(this.car.getId());
        car.setModel("4x4 Bronto");
        carService.update(this.car, car);

        Car updatedCar = carService.getById(this.car.getId());
        assertEquals(updatedCar.getMark(), this.car.getMark());
        assertEquals(updatedCar.getModel(), this.car.getModel());
    }

    @Test
    @Transactional
    @Rollback(true)
    public void delete() {
        Long id = this.car.getId();
        carService.delete(id);

        Car car = carService.getById(id);
        assertNull(car);
    }
}