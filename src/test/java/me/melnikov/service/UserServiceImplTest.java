package me.melnikov.service;

import com.sun.org.apache.xpath.internal.operations.Bool;
import me.melnikov.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceImplTest {

    @Autowired
    UserService userService;

    private User user;

    @Before
    public void setUp() throws Exception {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        String password = encoder.encode("444");
        this.user = new User("Alex", "Frolov", "afrolov", password, true);
        userService.create(user);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void getAll() {
        List<User> users = userService.getAll();
        assertEquals(4, users.size());
    }

    @Test
    @Transactional
    @Rollback(true)
    public void getById() {
        User user = userService.getById(this.user.getId());
        assertEquals(this.user.getLogin(), user.getLogin());
        assertEquals(this.user.getPassword(), user.getPassword());
        assertEquals(this.user.getFirstName(), user.getFirstName());
        assertEquals(this.user.getLastName(), user.getLastName());
        assertEquals(this.user.getRole(), user.getRole());
        assertEquals(this.user.getActive(), user.getActive());
    }

    @Test
    @Transactional
    @Rollback(true)
    public void create() {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        String password = encoder.encode("555");
        User user = new User("Oleg", "Pavlov", "opavlov", password, true);
        userService.create(user);

        User createdUser = userService.getById(user.getId());
        assertEquals(createdUser.getLogin(), user.getLogin());
        assertEquals(createdUser.getPassword(), user.getPassword());
        assertEquals(createdUser.getFirstName(), user.getFirstName());
        assertEquals(createdUser.getLastName(), user.getLastName());
        assertEquals(createdUser.getRole(), user.getRole());
        assertEquals(createdUser.getActive(), user.getActive());
    }

    @Test
    @Transactional
    @Rollback(true)
    public void update() {
        User user = userService.getById(this.user.getId());
        user.setFirstName("TestName");

        userService.update(this.user, user);
        assertEquals(this.user.getLogin(), user.getLogin());
        assertEquals(this.user.getPassword(), user.getPassword());
        assertEquals(this.user.getFirstName(), user.getFirstName());
        assertEquals(this.user.getLastName(), user.getLastName());
        assertEquals(this.user.getRole(), user.getRole());
        assertEquals(this.user.getActive(), user.getActive());
    }

    @Test
    @Transactional
    @Rollback(true)
    public void delete() {
        Long id = this.user.getId();
        userService.delete(id);

        User user = userService.getById(id);
        assertNull(user);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void itemExist() {
        Boolean bool = userService.itemExist(this.user.getId());

        assertTrue(bool);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void itemExist1() {
        Boolean bool = userService.itemExist(this.user);

        assertTrue(bool);
    }

    @Test
    @Transactional
    @Rollback(true)
    public void getByLogin() {
        User user = userService.getByLogin(this.user.getLogin());
        assertEquals(this.user.getLogin(), user.getLogin());
        assertEquals(this.user.getPassword(), user.getPassword());
        assertEquals(this.user.getFirstName(), user.getFirstName());
        assertEquals(this.user.getLastName(), user.getLastName());
        assertEquals(this.user.getRole(), user.getRole());
        assertEquals(this.user.getActive(), user.getActive());
    }
}