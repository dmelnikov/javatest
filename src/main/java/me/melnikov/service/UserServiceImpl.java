package me.melnikov.service;

import me.melnikov.model.Role;
import me.melnikov.model.User;
import me.melnikov.repository.UserRepository;
import org.hibernate.Hibernate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("userService")
public class UserServiceImpl implements UserService {
    private final UserRepository repository;

    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<User> getAll() {
        return repository.findAll();
    }

    @Override
    public User getById(Long id) {
        User result = repository.findOne(id);
        Hibernate.initialize(result);
        return result;
    }

    @Override
    public User create(User item) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String password = passwordEncoder.encode(item.getPassword());
        item.setPassword(password);
        return repository.save(item);
    }

    @Override
    public User update(User existItem, User newItem) {
        if (newItem.getFirstName() != null)
            existItem.setFirstName(newItem.getFirstName());
        if (newItem.getLastName() != null)
            existItem.setLastName(newItem.getLastName());
        if (newItem.getLogin() != null)
            existItem.setLogin(newItem.getLogin());
        if (newItem.getPassword() != null){
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            existItem.setPassword(passwordEncoder.encode(newItem.getPassword()));
        }
        if (newItem.getRole() != null)
            existItem.setRole(newItem.getRole());
        if (newItem.getActive() != null){
            existItem.setActive(newItem.getActive());
        }
        return repository.save(existItem);
    }

    @Override
    public void delete(Long id) {
        repository.delete(id);
    }

    @Override
    public Boolean itemExist(User user) {
        return repository.findByLogin(user.getLogin()) != null;
    }

    @Override
    public Boolean itemExist(Long userId) {
        return repository.exists(userId);
    }

    @Override
    public User getByLogin(String login) {
        return repository.findByLogin(login);
    }
}
