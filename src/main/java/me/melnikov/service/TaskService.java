package me.melnikov.service;

import me.melnikov.model.Task;

public interface TaskService extends BaseService<Task> {
}
