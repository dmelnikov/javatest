package me.melnikov.service;

import me.melnikov.model.Car;
import me.melnikov.model.Task;
import me.melnikov.repository.CarRepository;
import me.melnikov.repository.TaskRepository;
import org.hibernate.Hibernate;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service("taskService")
public class TaskServiceImpl implements TaskService {
    private final TaskRepository repository;

    public TaskServiceImpl(TaskRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Task> getAll() {
        return repository.findAll();
    }

    @Override
    public Task getById(Long id) {
        try {
            if (this.itemExist(id)){
                //todo: узнать почему если не найдено, вместо null кидает javax.persistence.EntityNotFoundException или JpaObjectRetrievalFailureException
                Task result = repository.getOne(id);
                if (result != null)
                    Hibernate.initialize(result);
                return result;
            }
            return null;
        }
        catch (JpaObjectRetrievalFailureException ex){
            return null;
        }
        catch (EntityNotFoundException ex){
            return null;
        }
    }

    @Override
    public Task create(Task item) { return repository.save(item); }

    @Override
    public Task update(Task existItem, Task newItem) {
        if (newItem.getDescription() != null)
            existItem.setDescription(newItem.getDescription());
        return repository.save(existItem);
    }

    @Override
    public void delete(Long id) {
        repository.delete(id);
    }

    @Override
    public Boolean itemExist(Task item) {
        return repository.findByDescriptionAndCar_Id(item.getDescription(), item.getCar().getId()) != null;
    }

    @Override
    public Boolean itemExist(Long itemId) {
        return repository.exists(itemId);
    }
}
