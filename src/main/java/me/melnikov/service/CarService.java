package me.melnikov.service;

import me.melnikov.model.Car;

public interface CarService extends BaseService<Car> {
}
