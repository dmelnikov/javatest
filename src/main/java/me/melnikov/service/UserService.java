package me.melnikov.service;

import me.melnikov.model.User;

public interface UserService extends BaseService<User> {
    User getByLogin(String login);
}
