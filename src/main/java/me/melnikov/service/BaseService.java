package me.melnikov.service;

import java.util.List;

public interface BaseService<T> {
    List<T> getAll();
    T getById(Long id);
    T create(T item);
    T update(T existItem, T newItem);
    void delete(Long id);
    Boolean itemExist(T item);
    Boolean itemExist(Long itemId);
}
