package me.melnikov.service;

import me.melnikov.model.Car;
import me.melnikov.repository.CarRepository;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("carService")
public class CarServiceImpl implements CarService {
    private final CarRepository repository;

    public CarServiceImpl(CarRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Car> getAll() {
        return repository.findAll();
    }

    @Override
    public Car getById(Long id) {
        Car result = repository.findOne(id);
        if (result != null)
            Hibernate.initialize(result);
        return result;
    }

    @Override
    public Car create(Car item) {
        return repository.save(item);
    }

    @Override
    public Car update(Car existItem, Car newItem) {
        if (newItem.getMark() != null)
            existItem.setMark(newItem.getMark());
        if (newItem.getModel() != null)
            existItem.setModel(newItem.getModel());
        return repository.save(existItem);
    }

    @Override
    public void delete(Long id) {
        repository.delete(id);
    }

    @Override
    public Boolean itemExist(Car item) {
        return repository.findByMarkAndModel(item.getMark(), item.getModel()) != null;
    }

    @Override
    public Boolean itemExist(Long itemId) {
        return repository.exists(itemId);
    }
}
