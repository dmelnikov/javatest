package me.melnikov;


import me.melnikov.model.Car;
import me.melnikov.model.Role;
import me.melnikov.model.User;
import me.melnikov.repository.CarRepository;
import me.melnikov.repository.TaskRepository;
import me.melnikov.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class MainApplicationClass implements CommandLineRunner {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private final UserRepository userRepository;
    private final CarRepository carRepository;
    private final TaskRepository taskRepository;

    @Autowired
    public MainApplicationClass(UserRepository userRepository, CarRepository carRepository, TaskRepository taskRepository) {
        this.userRepository = userRepository;
        this.carRepository = carRepository;
        this.taskRepository = taskRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(MainApplicationClass.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        List<User> users = new ArrayList<>(3);
        users.add(new User(1L, "Ivan", "Ivanov", "iivanov", passwordEncoder.encode("111"), true));
        users.add(new User(2L, "Petr", "Petrov", "ppetrov",passwordEncoder.encode("222"), false));
        users.add(new User(3L, "Vasily", "Sidorov", "vsidorov", passwordEncoder.encode("333"), Role.ROLE_ADMIN, true));

        List<Car> cars = new ArrayList<>(2);
        cars.add(new Car("Lada", "Vesta"));
        cars.add(new Car("Lada", "Kalina"));
        cars.add(new Car("Lada", "Granta"));

        userRepository.save(users);
        carRepository.save(cars);
    }
}
