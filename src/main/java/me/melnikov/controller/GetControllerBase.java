package me.melnikov.controller;

import me.melnikov.model.User;
import me.melnikov.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Collection;
import java.util.List;

public class GetControllerBase<T> {

    @Autowired
    BaseService<T> service;

    public GetControllerBase(BaseService<T> service) {
        this.service = service;
    }

    ResponseEntity<Collection<T>> index(){
        List<T> items = service.getAll();
        if (items.isEmpty())
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(items, HttpStatus.OK);

    }

    ResponseEntity<T> get(String id){
        T item = service.getById(Long.decode(id));
        if (item == null){
            System.out.println("Item with id: " + id + " not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(item, HttpStatus.OK);
    }
}
