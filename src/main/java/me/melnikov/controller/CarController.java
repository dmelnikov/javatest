package me.melnikov.controller;

import me.melnikov.model.Car;
import me.melnikov.service.BaseService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
public class CarController extends CrudControllerBase<Car> {

    public CarController(BaseService<Car> service) { super(service); }

    @GetMapping("/car")
    ResponseEntity<Collection<Car>> index(){ return super.index(); }

    @GetMapping("/car/{id}")
    ResponseEntity<Car> get(@PathVariable String id) {
        return super.get(id);
    }

    @PostMapping("/car")
    ResponseEntity<?> create(@RequestBody Car car){
        if (service.itemExist(car)) {
            System.out.println("Unable to create. Car with mark '" + car.getMark() + "' and model '" + car.getModel() + "' already exist");
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(service.create(car), HttpStatus.CREATED);
    }

    @PutMapping("/car/{id}")
    ResponseEntity<?> update(@PathVariable String id, @RequestBody Car car){ return super.update(id, car); }

    @DeleteMapping("/car/{id}")
    ResponseEntity<?> delete(@PathVariable String id){
        return super.delete(id);
    }
}
