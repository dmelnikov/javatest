package me.melnikov.controller;

import me.melnikov.model.User;
import me.melnikov.service.BaseService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

public abstract class CrudControllerBase<T> extends GetControllerBase<T> {

    public CrudControllerBase(BaseService<T> service) {
        super(service);
    }

    abstract ResponseEntity<?> create(T item);

    ResponseEntity<?> update(String id, T item){
        T existItem = super.service.getById(Long.decode(id));
        if (existItem == null) {
            System.out.println("Unable to update. Item with id: " + id + " not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(service.update(existItem, item), HttpStatus.OK);
    }

    ResponseEntity<?> delete(String id){
        Long itemId = Long.decode(id);
        if (service.itemExist(itemId)) {
            service.delete(itemId);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        }
        System.out.println("Unable to delete. Item with id: " + id + " not found");
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
