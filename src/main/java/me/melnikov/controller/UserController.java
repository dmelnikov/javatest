package me.melnikov.controller;

import me.melnikov.model.User;
import me.melnikov.service.BaseService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
public class UserController extends CrudControllerBase<User> {

    public UserController(BaseService<User> service) {
        super(service);
    }

    @Override
    @GetMapping("/user")
    ResponseEntity<Collection<User>> index() {
        return super.index();
    }

    @Override
    @GetMapping("/user/{id}")
    ResponseEntity<User> get(@PathVariable String id) {
        return super.get(id);
    }

    @PostMapping("/user")
    ResponseEntity<?> create(@RequestBody User user){
        if (service.itemExist(user)) {
            System.out.println("Unable to create. User with login '" + user.getLogin() + "' already exist");
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(service.create(user), HttpStatus.CREATED);
    }

    @PutMapping("/user/{id}")
    ResponseEntity<?> update(@PathVariable String id, @RequestBody User user){
        return super.update(id, user);
    }

    @DeleteMapping("/user/{id}")
    ResponseEntity<?> delete(@PathVariable String id){
        return super.delete(id);
    }
}
