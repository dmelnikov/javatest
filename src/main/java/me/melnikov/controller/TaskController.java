package me.melnikov.controller;

import me.melnikov.model.Car;
import me.melnikov.model.Task;
import me.melnikov.model.User;
import me.melnikov.repository.CarRepository;
import me.melnikov.repository.TaskRepository;
import me.melnikov.repository.UserRepository;
import me.melnikov.service.BaseService;
import me.melnikov.service.CarService;
import me.melnikov.service.TaskService;
import me.melnikov.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.List;

@RestController
public class TaskController extends GetControllerBase<Task> {

    @Autowired
    UserService userService;

    @Autowired
    CarService carService;

    public TaskController(BaseService<Task> service) {
        super(service);
    }

    @GetMapping("/task")
    ResponseEntity<Collection<Task>> index(){ return super.index(); }

    @GetMapping("/task/{id}")
    ResponseEntity<Task> get(@PathVariable String id) {
        return super.get(id);
    }

    @PostMapping("/task")
    ResponseEntity<?> create(HttpServletRequest request, @RequestBody Task task){
        if (service.itemExist(task)) {
            System.out.println("Unable to create. Task with some description already exist");
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        if (!carService.itemExist(task.getCar().getId())) {
            System.out.println("Unable to create. Car with id: " + task.getCar().getId() + " not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        User user = userService.getByLogin(request.getUserPrincipal().getName());
        task.setUser(user);
        return new ResponseEntity<>(service.create(task), HttpStatus.CREATED);
    }

    @PutMapping("/task/{id}")
    ResponseEntity<?> update(HttpServletRequest request, @PathVariable String id, @RequestBody Task task){
        Long taskId = Long.decode(id);
        Task existTask = service.getById(taskId);
        if (existTask == null) {
            System.out.println("Unable to update. Task with id: " + id + " not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        User user = userService.getByLogin(request.getUserPrincipal().getName());
        existTask.setUser(user);
        Car car = carService.getById(task.getCar().getId());
        existTask.setCar(car);
        return new ResponseEntity<>(service.update(existTask, task), HttpStatus.OK);
    }


    @DeleteMapping("/task/{id}")
    ResponseEntity<?> delete(@PathVariable String id){
        Long taskId = Long.decode(id);
        if (service.itemExist(taskId)) {
            service.delete(taskId);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        }
        System.out.println("Unable to delete. Task with id: " + id + " not found");
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
