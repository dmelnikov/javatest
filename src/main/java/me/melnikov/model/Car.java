package me.melnikov.model;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Car extends BaseEntity {
    @Column(nullable = false)
    private String mark;
    @Column(nullable = false)
    private String model;

    public Car() {
        super();
    }

    public Car(String mark, String model) {
        super();
        this.mark = mark;
        this.model = model;
    }

    public Car(Long id, String mark, String model) {
        super(id);
        this.mark = mark;
        this.model = model;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

}
