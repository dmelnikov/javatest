package me.melnikov.model;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class Task extends BaseEntity {

    @OneToOne
    @NotFound(action = NotFoundAction.IGNORE)
    private User user;

    private String description;

    @OneToOne
    @NotFound(action = NotFoundAction.IGNORE)
    private Car car;

    public Task() {
        super();
    }

    public Task(User user, String description, Car car) {
        super();
        this.user = user;
        this.description = description;
        this.car = car;
    }

    public Task(Long id, User user, String description, Car car) {
        super(id);
        this.user = user;
        this.description = description;
        this.car = car;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Car getCar() { return this.car; }

    public void setCar(Car car) {
        this.car = car;
    }
}
