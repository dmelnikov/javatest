package me.melnikov.repository;

import me.melnikov.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {
    Car findByMarkAndModel(String mark, String model);
}
