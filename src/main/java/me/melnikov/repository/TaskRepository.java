package me.melnikov.repository;

import me.melnikov.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task, Long> {
    Task findByDescriptionAndCar_Id(String description, Long carId);
}
